![PostgREST](images/logo-big.png) 

# What is PostgREST
- Automatic REST API backend server for any PostgreSQL database.
- Endpoints and their behaviour are determined by tables and constraints inside database. 
- Compatible with OpenAPI (formerly swagger).
- For more information see [https://postgrest.org/](https://postgrest.org/)

## PostgreSQL

- Postgres server image [ghusta/postgres-world-db](https://hub.docker.com/r/ghusta/postgres-world-db).
- This image contains publicly available *world* database.
    - MySQL version available [here](https://dev.mysql.com/doc/world-setup/en/).
- Access details from image:
    - database : *world-db*
    - user : *world*
    - password : *world123*

## pgAdmin
- Project contains popular administration and development WebUI for PostgreSQL servers and databases **pgAdmin**.
- Access to world-db is preconfigured in *servers.json*.
- You can change access details in enviromental variables:
    - login : *admin@admin.com*
    - password : *adminadmin*
    
![pgAdmin login](images/pgadmin-login.png)    
![pgAdmin select](images/pgadmin-select.png) 

## RapiDoc
- OpenAPI specification viewer of my preference.
- Alternative to [Swagger UI](https://swagger.io/tools/swagger-ui/).
- PostgREST uses [logical operators](https://postgrest.org/en/stable/api.html?highlight=logical#operators).

![RapiDoc API](images/rapidoc-api.png) 
![RapiDoc try](images/rapidoc-try-request.png) 
    

# Startup:
- **docker-compose**
    -
          docker-compose up -d
    - All services will be available on your localhost.
- **docker swarm**
    -
          docker stack deploy -c postgrest_swarm_stack.yml postgrest
    - Replace YOUR_SERVER_IP_OR_URL with your correct values inside *postgrest_swarm_stack.yml*.



# Helm chart:
```bash
ln -s docker-compose.yml postgrest-helm-chart.yml
kompose convert -c -f postgrest-helm-chart.yml
# Create secret, configMap and Ingresses, edit pgadmin deployment, change URLs according to ingresses.
helm upgrade --install postgrest ./postgrest-helm-chart --namespace postgrest --create-namespace
```